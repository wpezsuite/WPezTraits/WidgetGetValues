<?php

namespace WPez\WPezTraits\WidgetGetValue;

trait TraitGetValueForOptions {

	public function getValueForOptions( $arr_args = false ) {

		$arr_defaults = [
			'inst_new' => false,
			'inst_old' => false,
			'name'     => false,
			'options' => [],
			'default'  => ''
		];

		$arr = array_merge( $arr_defaults, $arr_args );

		if ( ! is_array($arr['inst_new']) ){
			return false;
		}
		if ( ! is_string( $arr['name']) ){
			return false;
		}

		if ( ! is_array($arr['options']) ){
			return $arr['default'];
		}

		if ( isset( $arr['inst_new'][ $arr['name']]) && isset( $arr['options'][ $arr['inst_new'][$arr['name']] ]) ){
			return $arr['inst_new'][ $arr['name']];
		}
		return $arr['default'];

	}
}