<?php

namespace WPez\WPezTraits\WidgetGetValue;

trait TraitGetValueElseDefault {

	public function getValueElseDefault( $arr_args = false ) {

		$arr_defaults = [
			'inst_new' => false,
			'inst_old' => false,
			'name'     => false,
			'sanitize' => 'strip_tags',
			'default'  => ''
		];

		$arr = array_merge( $arr_defaults, $arr_args );

		if ( ! is_array($arr['inst_new']) ){
			return false;
		}
		if ( ! is_string( $arr['name']) ){
			return false;
		}

		// TODO - moer args validation?

		$str_fn = trim( $arr['sanitize'] );

		if ( $arr['sanitize'] === false ) {
			// no sanitize?? - BE CAREFUL
			return ( ! empty( $arr['inst_new'][ $arr['name'] ] ) ) ? $arr['inst_new'][ $arr['name'] ] : $arr['default'];

		} elseif ( ! function_exists( $str_fn ) ) {
			// default sanitize
			$str_fn = 'strip_tags';
		}

		return ( ! empty( $arr['inst_new'][ $arr['name'] ] ) ) ? ( $str_fn( $arr['inst_new'][ $arr['name'] ] ) ) : $arr['default'];

	}
}
