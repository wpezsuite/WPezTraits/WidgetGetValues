<?php

namespace WPez\WPezTraits\WidgetGetValues;

trait TraitAllShared {

	use \WPez\WPezTraits\WidgetGetValue\TraitGetBool;
	use \WPez\WPezTraits\WidgetGetValue\TraitGetMultiForOptions;
	use \WPez\WPezTraits\WidgetGetValue\TraitGetNumber;
	use \WPez\WPezTraits\WidgetGetValue\TraitGetValueElseDefault;
	use \WPez\WPezTraits\WidgetGetValue\TraitGetValueForOptions;

}
